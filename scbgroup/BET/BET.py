import matplotlib.pyplot as pl
import numpy as np
import xlrd
import pandas as pd

def ASAPObj(filename):

    # function to extract cell range from boundary cells
    def get_cell_range(start_col, start_row, end_col, end_row):
        return [sh.row_slice(row, start_colx=start_col, end_colx=end_col+1) 
                for row in range(start_row, end_row+1)]
                
    # function to extract dataset title and metadata
    def getmeta(tsv):
        from io import StringIO

        buf=StringIO(tsv)
        lines = buf.read().splitlines()
        
        # get line number of "Comments:" line. Title is assumed to be the following line of
        # non-zero length.
        icomment= [ii for ii, l in enumerate(lines) if l.startswith('Comments:')][0]

        title=''

        for line in lines[icomment+1:]:
            stripline=line.strip('\t')

            if len(stripline) > 0:
                title=stripline
                break
        
        meta= '\n'.join(lines[0:icomment])

        return title, meta    
    
    # function to get first row containing all numbers- this is the start of data.
    def getinum(tsv):
        a=[]

        import re
        from io import StringIO

        buf=StringIO(tsv)
        lines = buf.read().splitlines()
    
        pattern=re.compile('^[+-]?([0-9]*[.])?[0-9]+')
        inum= [ii for ii, l in enumerate(lines) if re.match(pattern,l)][0]
    
        pattern_ads=re.compile('Adsorption')
        inum_ads=[ii for ii , l in enumerate(lines) if re.search(pattern_ads,l)]
    
        if inum_ads == a:
            inum_ads=0
        else:
            inum_ads=inum_ads[0]
    
        return inum,inum_ads

    # load workbook and first sheet
    book=xlrd.open_workbook(filename,encoding_override='cp1252')
    sh = book.sheet_by_index(0)
    
    # determine number of records by counting borders defined by '|'
    bfilter= lambda icol: sh.cell_value(rowx=0, colx=icol) is '|'
    borders=[c for c in range(sh.ncols) if bfilter(c)]
 
    # extract cell ranges into list (length= number of records)
    rl=[]
    if len(borders)>0:
        rh=get_cell_range(0, 0, borders[0]-1, sh.nrows-1)
        rl+=[rh]
        rs=[get_cell_range(borders[ib]+1, 0, borders[ib+1]-1, sh.nrows-1) for ib, b in enumerate(borders[:-1])]
        rl += rs
        rlast=get_cell_range(borders[-1]+1, 0, sh.ncols , sh.nrows-1)
        rl.append(rlast)
    else: # file has just one dataset
        rs=[get_cell_range(0, 0, sh.ncols , sh.nrows-1)]
        rl += rs

    # convert records from list to TSV
    tsvl=[]
    for therange in rl:
        tsv=''
        for r in therange:
            rt=[str(c.value) for c in r]
            tsv += '\t'.join(rt) + '\n'

        tsvl += [tsv]
    
    data=[getmeta(tsv) for tsv in tsvl]
    titles,metas= map(list, zip(*data))
    
    # operate on each item in tsvl one at a time
    
    d={}
    b=borders+[sh.ncols]
    root=[0]+[e+1 for e in b]
    
    for ii,tsv in enumerate(tsvl):
        if titles[ii]=='Summary Report':  
            d[titles[ii]]=tsv
        else:
            n,nads=getinum(tsv)
            cols= range(root[ii],root[ii+1]-1)
            df=pd.read_excel(book,engine='xlrd', skiprows=n-1, usecols=cols)
            
            if nads!=0:
                ads=df.iloc[:,0:2] # split data
                des=df.iloc[:,2:4]
                des.columns=list(ads)
                df=ads.append(des) # append in one df
                
            df=df.dropna([0,1],how='all')  # eliminate rows & cols with all NaN
            df.meta=metas[ii]  # retain text from data header
            d[titles[ii]]=df
        
    return d
    
def ASAPPlot(d,dkey):
    df=d[dkey]
    x=df.iloc[:,0]
    y=df.iloc[:,1]
    
    pl.semilogx(x,y,'-')
    pl.xlabel(df.keys()[0])
    pl.ylabel(df.keys()[1])
    pl.title(dkey)
    pl.show()


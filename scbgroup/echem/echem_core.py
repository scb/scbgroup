import numpy as np
import matplotlib.pyplot as plt

# ignore warning from pandas when attaching metadata to dataframe
import warnings
warnings.simplefilter(action='ignore', category=UserWarning)
import pandas as pd


def read_mpt(filename):
    import re
    
    # load metadata
    class struct():
        pass
    meta=struct()

    pattern=re.compile('Nb header lines : (\d+)')

    with open(filename,encoding="latin-1") as f:
        lines=f.readlines()
        for line in lines:
            for match in re.finditer(pattern, line):
                meta.metalines=int(match.group(1))-1
                break
    meta.string=''.join( lines[:meta.metalines] )

    def index_containing_substring(the_list, substring):
        for i, s in enumerate(the_list):
            if substring in s:
                  return s
        return -1
            
    df=pd.read_csv(filename,sep='\t',header=0,skiprows=meta.metalines)

    # this is where the ignored warning appears
    df.meta=meta
    
    columns=list(df.columns.values)
    df.vcol=index_containing_substring(columns, 'Ewe')
    df.icol=index_containing_substring(columns, '<I>')
    df.tcol=index_containing_substring(columns, 'time')
    df.ccol=index_containing_substring(columns, 'cycle')

    # get scan rate from meta
    pattern="dE/dt \s* (\d*[.]\d*)"
    m=re.search(pattern,df.meta.string)

    df.meta.vscan=float(m.group(1))

    pattern="dE/dt unit \s* (.*)/s"
    m=re.search(pattern,df.meta.string)
    unit=m.group(1)

    if unit == 'mV':
        df.meta.vscan /= 1000  # V
    
    # get surface area from meta
    pattern="Electrode surface area : (\d*[.]\d*) cm"
    m=re.findall(pattern,df.meta.string)
    df.meta.area=float(m[-1])  # cm2
    
    # Calculate Current Density
    pattern="<I>/(.+)"
    m=re.search(pattern,df.icol)
    Iunit=m.group(1)
    df.idcol='<i>/' + Iunit + 'cm2'
    df[df.idcol]=df[df.icol]/df.meta.area  # mA/cm2

    return df
    
    
def plot_cv(df,cycle=0,current=False):
                
    vcol, tcol, ccol= df.vcol, df.tcol, df.ccol
    
    col= df.icol if current else df.idcol
            
    if cycle is not 0:
        v=df[df[ccol].isin(cycle)][vcol]
        icd=df[df[ccol].isin(cycle)][col]
        DoLegend=True
    else:
        v=df[vcol]
        icd=df[col]
        DoLegend=False
        
    plt.plot(v,icd)
    plt.xlabel(vcol)
    plt.ylabel(col)
    
    if DoLegend:
        plt.legend(['Cycle '+str(cycle)])

def calc_capacitance(df,cycle,vrange=[-5,5],DoPlot=False):

    from scipy.interpolate import interp1d

    mask= ( df[df.ccol]==cycle ) & ( df[df.vcol] >= vrange[0] ) & ( df[df.vcol] <= vrange[1] )

    # version for multiple cycles (not yet implemented):
    # mask= ( df[df.ccol].isin(cycle) ) & ( df[df.vcol] >= vrange[0] ) & ( df[df.vcol] <= vrange[1] )

    dfm=df[mask]

    dhi=dfm[dfm['ox/red']==1]
    vhi=dhi[df.vcol]
    ihi=dhi[df.idcol]

    dlo=dfm[dfm['ox/red']==0]
    vlo=dlo[df.vcol]
    ilo=dlo[df.idcol]

    vmin=max([min(vlo),min(vhi)])
    vmax=min([max(vlo),max(vhi)])

    L=len(vhi)
    vf=np.linspace(vmin,vmax,L)
    
    fhi=interp1d(vhi,ihi)
    flo=interp1d(vlo,ilo)

    ihif=fhi(vf)
    ilof=flo(vf)

    #convert to A/cm2
    if df.idcol=='<i>/mAcm2':
        ihif /= 1000  # A/cm2
        ilof /= 1000  # A/cm2

    #icap= C * vscan
    C=(ihif-ilof)/2/df.meta.vscan  * 1E6  # uF/cm2
    Cavg=np.mean(C)
    Cerr=np.std(C)

#     print( "{0} ± {1}".format(Cavg,Cerr) )
#     print( "{0} ± {1}".format(Cavg/25,Cerr/25) )
    
    if DoPlot:
        plt.subplot(2,1,1)
        plot_cv(df,[cycle])
        plt.plot(vhi,ihi,'-o',label='→')
        plt.plot(vlo,ilo,'-o',label='<-')
        plt.legend();
        
        plt.subplot(2,1,2)
        plt.plot(vf,C)
        plt.xlabel(df.vcol)
        plt.ylabel('Capacitance / µF cm$^{-2}$')
        plt.ylim(0,plt.ylim()[1]);
        
    return (Cavg,Cerr,vf,C)


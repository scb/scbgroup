
# coding: utf-8

# In[18]:

import pylab as py
import lmfit  # https://lmfit.github.io/lmfit-py/model.html
import numpy as np
import os
import scipy as sp
from lmfit import Model
import pandas as pd


def approach_fun(L, E=0.2, E0=0, D=1e-5, k0=1, rtip=25e-7, n=1,\
                 alpha=0.5, T=293, pn=0, ka=1e-5, ro=0):
    """"
      
        Tip approach curve equation derived for kinetic and tip shape dependence [1-3].

        ## (1) Mirkin, M. V.; Fan, F.-R. F.; Bard, A. J. 
        Scanning Electrochemical Microscopy Part 13. 
        Evaluation of the Tip Shapes of Nanometer Size Microelectrodes. 
        J. Electroanal. Chem. 1992, 328 (1–2), 47–62.

        ## (2) Liu, B.; Bard, A. J. 
        Scanning Electrochemical Microscopy. 45. 
        Study of the Kinetics of Oxygen Reduction on Platinum with 
        Potential Programming of the Tip. 
        J. Phys. Chem. B 2002, 106 (49), 12801–12806.

        ## (3) Mirkin, M. V.; Richards, T. C.; Bard, A. J. 
        Scanning Electrochemical Microscopy. 20. 
        Steady-State Measurements of the Fast Heterogeneous Kinetics 
        in the Ferrocene/Acetonitrile System. 
        J. Phys. Chem. 1993, 97 (29), 7672–7677.
        
        Parameters
        ----------
        L : int
            Dimensionless Distance from substrate
        E : int
            Applied Potential / V
        E0 : int
            Formal Potential / V 
        D : int
            Diffusion coefficient / cm2/s
        k0 : int
            First order rate constant arrhenius frequency factor
        rtip : int 
            tip radius / cm
        n : int 
            number of electrons
        alpha : int
            charge transfer coefficient (Butler-Volmer equation)
        T : int
            Temperature / K
        pn : int 
            binary approach curve switch for positive or negative (see below)
        ka : int 
            tip aspect ratio height/base radius (0: disk, 1: isosceles right cone)
        ro : int
            binary reduction or oxidation switch

        binary variable for positive or negative approach curves:
            positive approach curve, pn: 0
            negative approach curve, pn: 1

        binary variable for reduction or oxidation kinetics:
            reduction reaction, ro: 0
            oxidation reaction, ro: 1
"""
        
        

    F = 96485  # Faraday Constant | C/mol
    R = 8.314  # Gas Constant | J/mol*K
    f = F/(R*T)
    n = int(n)
    pn = int(pn)
    ro = int(ro)
    I = np.empty(len(L))
    # coefficients for analytic fit for positive feedback
    alpha_p = 0.68
    beta_p = 0.78377
    eta_p = 0.3315
    eps_p = 1.0672

    # coefficients for analytic fit for negative feedback
    alpha_n = 0.15
    beta_n = 1.5385
    eta_n = 0.58
    gamma_n = -1.14
    eps_n = 0.0908
    tau_n = 6.3
    omega_n = 1.017


    # effective mass transfer coefficient
    m0=(4*D*(alpha_p+beta_p/L+eta_p*py.exp(eps_p/L)))/(py.pi*rtip)

    # rate constant
    if ro==0:
        k=k0*py.exp((-alpha)*n*f*(E-E0))/m0
        s=1
    elif ro==1:
        k=k0*py.exp((1-alpha)*n*f*(E-E0))/m0
        s=-1
    if pn==0: 
        # positive approach
        # Dimensionless Current
        I=((2/(ka**2))*(((alpha_p+eta_p)/2)*ka**2+(beta_p-eta_p*eps_p)*ka+\
                                  (eta_p*eps_p*L-beta_p*L)*np.log(1+ka/L)))\
        /(1+np.exp(s*n*f*(E-E0))+1/k)

    elif pn==1:
        # negative approach

        psi_n = beta_n + eta_n*gamma_n+eps_n*tau_n/omega_n
        lambda_n = alpha_n+eta_n+eps_n+eps_n/omega_n
        # Dimensionless Current
        I = (1/lambda_n) - (2*psi_n/(lambda_n**2*ka)) +\
             ((2/(ka**2))*(psi_n**2/lambda_n**3+psi_n*L/lambda_n**2)*\
             py.log((lambda_n*ka/(lambda_n*L+psi_n))+1))  

    return I
        
        
class approach(object):
    def __init__(self,L=0, E=0.2, E0=0, D=1e-5, k0=1, rtip=25e-7, n=1,\
                 alpha=0.5, T=293, pn=0, ka=1e-5, ro=0):
        """
        Parameters
        ----------
        L : int
            Dimensionless Distance from substrate
        E : int
            Applied Potential / V
        E0 : int
            Formal Potential / V 
        D : int
            Diffusion coefficient / cm2/s
        k0 : int
            First order rate constant arrhenius frequency factor
        rtip : int 
            tip radius / cm
        n : int 
            number of electrons
        alpha : int
            charge transfer coefficient (Butler-Volmer equation)
        T : int
            Temperature / K
        pn : int 
            binary approach curve switch for positive or negative (see below)
        ka : int 
            tip aspect ratio height/base radius (0: disk, 1: isosceles right cone)
        ro : int
            binary reduction or oxidation switch

        binary variable for positive or negative approach curves:
            positive approach curve, pn: 0
            negative approach curve, pn: 1

        binary variable for reduction or oxidation kinetics:
            reduction reaction, ro: 0
            oxidation reaction, ro: 1

        """
        
        self.L = L
        self.E = E
        self.E0 = E0
        self.D = D
        self.k0 = k0
        self.rtip = rtip
        self.alpha = alpha
        self.T = T
        self.ka = ka

        F = 96485  # Faraday Constant | C/mol
        R = 8.314  # Gas Constant | J/mol*K
        self.F = F
        self.R = R
        self.f = self.F/(self.R*self.T)
        self.n = int(n)
        self.pn = int(pn)
        self.ro = int(ro)
        self.I = np.empty(len(self.L))
        
        #self.nn = nn
    def ume(self,C,shape):
        """
        ultra-micro electrode bulk current / A
        
        --------------------------------------
        C: int
            Bulk concentration of redox species / M
        shape : str
            shape of the electrode (options: disk, sphere, cylinder, cone)
        
        Cynthia G. Zoski, Michael V. Mirkin, Steady-State Limiting Currents 
        at Finite Conical Microelectrodes. 2002.
        
        Bard, A. J.; Faulkner, L. R.; York, N.; @bullet, C.; Brisbane, W.; 
        Toronto, S. E. ELECTROCHEMICAL METHODS Fundamentals and Applications; 1944.
        
        """
        
        cyl=2*self.D/(self.rtip)
        disk=4*self.D/(np.pi*self.rtip)
        hemi= self.D/self.rtip
        C=C/1000 # conversion from M to mol/cm^3
        
        #i_bulk=self.n*self.F*A*m*C
        
        spherefun = lambda C: self.n*self.F*(4*np.pi*self.rtip**2)*(self.D/self.rtip)*C
        diskfun = lambda C: self.n*self.F*(np.pi*self.rtip**2)*(4*self.D/(np.pi*self.rtip))*C
        cylfun = lambda C: self.n*self.F*(np.pi*self.rtip**2+2*np.pi*self.rtip**2*self.ka)*\
                (2*self.D/self.rtip)*C
        conefun = lambda C: self.n*self.F*(np.pi*self.rtip*(self.rtip+((self.ka*self.rtip)**2\
                +self.rtip**2)**(1/2)))*(self.D/self.rtip*(1+0.30661*self.ka**1.14466))*C
        
        options = {'sphere': spherefun,
                   'disk' : diskfun,
                   'cylinder' : cylfun,
                   'cone' : conefun
                    }
        
        return options[shape](C)

    def approach(self, L, nn= False, E=0.2, E0=0, D=1e-5, k0=1, rtip=25e-7, n=1,\
                 alpha=0.5, T=293, pn=0, ka=1e-5, ro=0):
        """
        Tip approach curve equation derived for kinetic and tip shape dependence [1-3].

        ## (1) Mirkin, M. V.; Fan, F.-R. F.; Bard, A. J. 
        Scanning Electrochemical Microscopy Part 13. 
        Evaluation of the Tip Shapes of Nanometer Size Microelectrodes. 
        J. Electroanal. Chem. 1992, 328 (1–2), 47–62.

        ## (2) Liu, B.; Bard, A. J. 
        Scanning Electrochemical Microscopy. 45. 
        Study of the Kinetics of Oxygen Reduction on Platinum with 
        Potential Programming of the Tip. 
        J. Phys. Chem. B 2002, 106 (49), 12801–12806.

        ## (3) Mirkin, M. V.; Richards, T. C.; Bard, A. J. 
        Scanning Electrochemical Microscopy. 20. 
        Steady-State Measurements of the Fast Heterogeneous Kinetics 
        in the Ferrocene/Acetonitrile System. 
        J. Phys. Chem. 1993, 97 (29), 7672–7677.

        """ 

        if nn is True:
            self.L = L
            self.E = E
            self.E0 = E0
            self.D = D
            self.k0 = k0
            self.rtip = rtip
            self.alpha = alpha
            self.ka = ka
        
        # coefficients for analytic fit for positive feedback
        alpha_p = 0.68
        beta_p = 0.78377
        eta_p = 0.3315
        eps_p = 1.0672

        # coefficients for analytic fit for negative feedback
        alpha_n = 0.15
        beta_n = 1.5385
        eta_n = 0.58
        gamma_n = -1.14
        eps_n = 0.0908
        tau_n = 6.3
        omega_n = 1.017


        # effective mass transfer coefficient
        m0=(4*self.D*(alpha_p+beta_p/self.L+eta_p*np.exp(eps_p/self.L)))/(np.pi*self.rtip)

        # rate constant
        if self.ro==0:
            k=self.k0*np.exp((-self.alpha)*self.n*self.f*(self.E-self.E0))/m0
            s=1
        elif self.ro==1:
            k=self.k0*np.exp((1-self.alpha)*self.n*self.f*(self.E-self.E0))/m0
            s=-1
        if self.pn==0: 
            # positive approach
            # Dimensionless Current
            self.I=((2/(self.ka**2))*(((alpha_p+eta_p)/2)*self.ka**2+(beta_p-eta_p*eps_p)*self.ka+\
                            (eta_p*eps_p*self.L-beta_p*self.L)*np.log(1+self.ka/self.L)))\
            /(1+np.exp(s*self.n*self.f*(self.E-self.E0))+1/k)

        elif self.pn==1:
            # negative approach

            psi_n = beta_n + eta_n*gamma_n+eps_n*tau_n/omega_n
            lambda_n = alpha_n+eta_n+eps_n+eps_n/omega_n
            # Dimensionless Current
            self.I = (1/lambda_n) - (2*psi_n/(lambda_n**2*self.ka)) +\
                ((2/(self.ka**2))*(psi_n**2/lambda_n**3+psi_n*self.L/lambda_n**2)*\
                 np.log((lambda_n*self.ka/(lambda_n*self.L+psi_n))+1))  

        return
    def approach_fit(self,n_lh=[0,4],k0_lh=[1e-12,1e2],ka_lh=[1e-5,10],D_lh=[1e-7,1e-3],rtip_lh=[1e-7,250e-7],\
                     E_fit=False, E0_fit=False, D_fit=False, rtip_fit=False, alpha_fit=False,\
                     T_fit=False, pn_fit=False, ka_fit=False,ro_fit=False,n_fit=False,k0_fit=False):

        fmodel=Model(approach_fun,nan_policy='omit')
        # initial values
        fmodel.set_param_hint('D', value=self.D, min=D_lh[0], max=D_lh[1])
        fmodel.set_param_hint('n', value=self.n, min=n_lh[0], max=n_lh[1])
        fmodel.set_param_hint('k0', value=self.k0, min=k0_lh[0], max=k0_lh[1])
        fmodel.set_param_hint('ka', value=self.ka, min=ka_lh[0], max=ka_lh[1])
        #fmodel.set_param_hint('nn', value=nn)
        fmodel.set_param_hint('E', value=self.E)
        fmodel.set_param_hint('E0', value=self.E0)
        fmodel.set_param_hint('rtip', value=self.rtip, min=rtip_lh[0], max=rtip_lh[1])
        fmodel.set_param_hint('alpha', value=self.alpha)
        fmodel.set_param_hint('T', value=self.T)
        fmodel.set_param_hint('pn', value=self.pn)
        fmodel.set_param_hint('ro', value=self.ro)
        

        #params = fmodel.make_params(nn=False, E=self.E, E0=self.E0, D=self.D, rtip=self.rtip,\
        #alpha=self.alpha, T=self.T, pn=self.pn, ka=self.ka,ro=self.ro)   not working for some reason, can still set hint values
        params = fmodel.make_params(E=self.E)
        
        # fix parameters:
        #params['nn'].vary = False
        params['alpha'].vary = alpha_fit
        params['D'].vary = D_fit
        params['E0'].vary = E0_fit
        params['E'].vary = E_fit
        params['ka'].vary = ka_fit
        params['n'].vary = n_fit
        params['pn'].vary = pn_fit    
        params['ro'].vary = ro_fit
        params['rtip'].vary = rtip_fit
        params['T'].vary = T_fit
        params['k0'].vary = k0_fit

        #fit
        results = fmodel.fit(self.I, params, L=self.L)
        return results

    def approach_plot(self):
        """
        Plot of normalized approach curve data

        Parameters
        ----------
        L : int
            Dimensionless Distance from substrate
        E : int
            Applied Potential
        E0 : int
            Formal Potential
        D : int
            Diffusion coefficient
        k0 : int
            First order rate constant arrhenius frequency factor
        rtip : int 
            tip radius
        n : int 
            number of electrons
        alpha : int
            charge transfer coefficient (Butler-Volmer equation)
        T : int
            Temperature
        label : string
            Legend entry

        """
        
        if self.pn == 0:
            ym = 4 # max plot current: 4 for normalized current 
        elif self.pn == 1:
            ym = 1.1 
        
        py.plot(self.L,self.I)
        py.xlabel('L',fontsize=16)
        py.ylabel('$i_T$(L)/$i_T,_\infty$',fontsize=16)
        py.ylim(0,ym)
        py.rc('xtick',labelsize=16)
        py.rc('ytick',labelsize=16)
        return

    #def tafel(E,E0,n,a,T,k=1):
        #
    #    F = 96485  # Faraday Constant | C/mol
    #    R = 8.314 # Gas Constant | J/mol*K
    #    I=n*F*k*py.exp(a*F*(E-E0)/(R*T))
    #    return I

    def load(self,filename, directory, sr=1):
        """
        load data from directory for analysis

        Parameters
        -------------
        filename : str
            filename of text data
        directory : str
            location of text file
        sr : int
            number of rows to skip, default is 1 (headers)
        """
        os.chdir(directory)
        os.listdir()
        data = np.loadtxt(filename , skiprows=sr)
        return data
    
    def set(self, attr, value):
        """
        set parameter values
        """
        if attr in self.__dict__.keys():
            self.__dict__[attr]=value
        else:
            raise ValueError("attr doesn't exist: " +  attr)
            
#from os.path import abspath, dirname, join

#import mahotas as mh  # noqa
#import matplotlib.pyplot as plt  # noqa
#import numpy as np

import pylab as py
import lmfit  # https://lmfit.github.io/lmfit-py/model.html
import numpy as np
import nano as ecs

from ..core import Image

#def test_tafel():
#    E0 = 0
#    E = 0.3
#    D = 1e-5
#    k0 = 1
#    a = 0.5
#    I=ecs.tafel(E,E0,n,a,T,k=1)
#    assert np.isclose(I[0],6.3)
#    return

def test_approach():
    k0=1 # Rate Constant | cm/s
    D=10E-6 # Diffusion Coefficient | cm2/s
    E=0.1 # applied potential | V
    E0=0.4 # formal potential | V
    alpha=0.5  # Charge transfer coefficient
    F=96485  # Faraday Constant | C/mol
    R=8.314 #Gas Constant | J/mol*K
    T=293 #Temperature | K
    f=F/(R*T)
    n=1  # number of electrons per reaction 
    rtip=25e-7 # Tip radius | cm
    L=5 # Dimensionless Distance 
    pn=0 #approach curve binary choice

    ka=0.0001 #tip aspect ratio
    I=ecs.approach(x_f, E, E0=E0_fit, D=D, rtip=rtip, alpha=alpha, T=T, pn=pn, k0=k0_fit, n=n_fit,ka=ka_fit,ro=1)
    assert np.isclose(I,1)
    return

#def test_rgb2gray():
#    default = abspath(join(dirname(__file__),
#                           'PtnanoCenk0.5et_171218_AM.0_00002_1.png'))
#    img2 = Image(default, 3)
#    img2.image = np.array([[[255, 231, 97], [45, 23, 18]]])
#    test_output = img2.rgb2gray()
#    assert np.isclose(test_output[0, 0], 222.9), 'bad grayscale'
#    assert np.isclose(test_output[0, 1], 29.008)
#    return


#def test_apply_threshold():
#    default = abspath(join(dirname(__file__),
#                           'PtnanoCenk0.5et_171218_AM.0_00002_1.png'))
#    img2 = Image(default, 3)
#    img2.image = np.array([[[300, 231, 97], [45, 23, 18]]])
#    img2.apply_threshold()
#    test_output2 = img2.image
#    assert np.isclose(test_output2[0, 0, 0], 255), 'bad thresholding'
#    assert np.isclose(test_output2[0, 1, 2], 0)
#    return

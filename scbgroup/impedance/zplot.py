def zplot(z,f=None,dowhat='both'):

# 
# zplot(z,f,dowhat)
# 
# Makes Bode and Nyquist plots of complex impedance data.
# Inputs:
# 	z:	complex impedance values (column format.) Multiple columns plotted
# 	    separately.
# 	f:  frequency values
# 	 q
# If only z is input, a Nyquist plt.plot is drawn.  If z and f are input, both
# Nyquist and Bode plots are drawn.  Use dowhat='bode' to draw just a Bode
# plt.plot.  Use dowhat='RI' to plt.plot np.real and np.imaginary parts vs. frequency.
# scb 5/10/2005

	import numpy as np
	import matplotlib.pyplot as plt

	def nyquist(z):
		zr=np.real(z)
		zi=np.imag(z)
		zmin=np.min(zr-zi)
		zmax=max(zr)
		dz=np.abs(zmax-zmin)/20
		zmin=zmin-dz
		zmax=zmax+dz

		plt.plot(zr,-zi,'-o')

		# hold on
		# color='rgbmcrgbmcrgbmc'
		# marker='os*dxos*dxos*dx'
		# for ii=2:size(z,2)
		#     pstyle=['-' marker[ii] color[ii]]
		#     plt.plot(zr(:,ii),-zi(:,ii),pstyle)
		# hold off

		plt.xlim(zmin, zmax)
		plt.ylim(zmin, zmax)

		plt.axis('equal')

		plt.axhline(linestyle=':',lw=0.5, color='gray')
		plt.axvline(linestyle=':',lw=0.5, color='gray')

		plt.xlabel('$Z_R$ / $\Omega$')
		plt.ylabel('$-Z_I$ / $\Omega$')
		plt.show()    


	def bode(z,f):
		zr=np.real(z)
		zi=np.imag(z)
		phi=-180/np.pi*np.angle(z)
		zmin=np.min(zr-zi)
		zmax=max(zr)
		dz=np.abs(zmax-zmin)/20
		zmin=zmin-dz
		zmax=zmax+dz

		plt.subplot(2,1,1)
		plt.semilogx(f,np.abs(z),'-o')
		plt.xlabel('Frequency, Hz')
		plt.ylabel('| Z |')
		plt.xlim(0,plt.xlim()[1])
		plt.ylim(0,plt.ylim()[1])

		plt.subplot(2,1,2)
		plt.semilogx(f,phi,'-o')
		plt.xlabel('Frequency, Hz')
		plt.ylabel(r'-$\theta$, degrees')
		plt.show()


	def ri(z,f):
		zr=np.real(z)
		zi=np.imag(z)
		plt.semilogx(f,zr,'-o',f,-zi,'-o')
		plt.xlabel('Frequency, Hz')
		plt.ylabel('$Z_r$')
		plt.legend(['$Z_r$','-$Z_i$'])
		plt.show()

	if (not np.any(dowhat in ['both','RI','IR'])):
		dowhat=dowhat.lower()
	if np.any(dowhat in ['ri','Ri','rI','ir','iR','Ir']):
		dowhat=dowhat.upper()
  
	if f is None:
		 nyquist(z)     

	else:
		if dowhat == 'nyquist' :
			nyquist(z)

		elif dowhat == 'both' :
			nyquist(z)
			bode(z,f)

		elif dowhat == 'bode' :
			bode(z,f)

		elif (dowhat == 'IR') | (dowhat == 'RI') :
			ri(z,f)
		
		else:
			print (dowhat + ': Do nothing!')


scbgroup package
==============================

This is a collection of functions for data analysis is electrochemistry.  Current packages include:

 - scbgroup.echem : electrochemical analysis
 - scbgroup.impedance : impedance analysis
 - scbgroup.BET : BET analysis  
 
 For more information, email scb at msu dot edu
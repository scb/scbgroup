from setuptools import setup, find_packages

setup(
   name='scbgroup',
   version='1.0',
   description='analysis tools for the group',
   author='Scott Calabrese Barton',
   author_email='scb@msu.edu',
   packages = find_packages(),  #same as name
   install_requires=['numpy', 'matplotlib', 'pandas','lmfit'], #external packages as dependencies
)